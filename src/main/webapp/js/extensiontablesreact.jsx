class ExtensionTableRow extends React.Component {
    render() {
        const {extensionTable} = this.props
        return (
            <tr key="extensionTableRow">
                <td>{extensionTable.name}</td>
                <td>
                    <button onClick={() => viewExtensionTableColumn(extensionTable.name)}>View Columns</button>
                    <button onClick={() => deleteExtensionTable(extensionTable.name)}>Delete</button>
                </td>
            </tr>
        )
    }
}

function ExtensionTable(props) {
    const {extensionTables} = props
    return (
        <table key="extensionTableReact">
            <thead>
            <tr>
                <th>Name</th>
                <th>Action</th>
            </tr>
            </thead>
            <tbody>
            {extensionTables.map((extensionTable) => (
                <ExtensionTableRow extensionTable={extensionTable}/>
            ))}
            </tbody>
        </table>
    )
}

class ExtensionTableColumnRow extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            tableName: props.tableName,
            columnName: props.extensionTableColumn
        }
        this.handleDelete = this.handleDelete.bind(this);
    }

    handleDelete() {
        const columnInfo = {
            "columnName": this.state.columnName,
            "tableName": this.state.tableName
        }
        deleteExtensionTableColumn(columnInfo)
    }

    render() {
        return (
            <tr>
                <td>{this.props.extensionTableColumn}</td>
                <td>
                    <button onClick={this.handleDelete}>Delete</button>
                </td>
            </tr>
        )
    }
}

function ExtensionTableColumns(props) {
    const {extensionTableColumns} = props

    return (
        <table id="extensionTableColumnsReact">
            <thead>
            <tr>
                <th>Name</th>
                <th>Action</th>
            </tr>
            </thead>
            <tbody>
            {extensionTableColumns.map((extensionTableColumn) => (
                <ExtensionTableColumnRow key={extensionTableColumn.name} extensionTableColumn={extensionTableColumn.name} tableName={props.extensionTableName} />
            ))}
            </tbody>
        </table>
    )
}

function extensionTableContent() {
    getJSON('http://localhost:8080/ProjectAsterix/rest/extensiontable/')
        .then(data => {
            ReactDOM.render(<ExtensionTable extensionTables={data}/>, document.getElementById('ExtensionTables'))
        });
}
extensionTableContent()


class AddingNewExtensionTableColumn extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            tableName: "empty",
            columnName: "empty",
            columnType: "INTEGER"
        }
        this.handleInputChange = this.handleInputChange.bind(this);
        this.handleSave = this.handleSave.bind(this);
    }

    handleInputChange(event) {
        let change = {};
        change[event.target.name] = event.target.value;
        this.setState(change);
    }

    handleSave() {
        const newColumnInfo = {
            "tableName": this.state.tableName,
            "columnName": this.state.columnName,
            "columnType": this.state.columnType
        }
        addNewExtensionTableColumn(newColumnInfo)
    }

    render() {
        return (
            <div>
                <tr><input type="text" name="tableName" placeholder="Table Name"
                           onChange={this.handleInputChange}/></tr>
                <tr><input type="text" name="columnName" placeholder="Column Name"
                           onChange={this.handleInputChange}/></tr>
                <tr>
                    <select
                        name="columnType" id="columnTypeSelect" required onChange={this.handleInputChange}>
                        <option value="INTEGER">Integer</option>
                        <option value="DECIMAL">Decimal</option>
                        <option value="VARCHAR">Varchar</option>
                        <option value="TEXT">Text</option>
                        <option value="TIMESTAMP">Timestamp</option>
                    </select>
                </tr>
                <tr>
                    <button onClick={this.handleSave}>Save</button>
                </tr>
            </div>
        )
    }
}

function NewExtensionTableColumn() {
    return (
        <table id="NewColumn">
            <thead>
            <tr>
                <th>Add new column</th>
            </tr>
            </thead>
            <tbody>
            <AddingNewExtensionTableColumn/>
            </tbody>
        </table>
    )
}

function renderExtensionTable() {
    ReactDOM.render(<NewExtensionTableColumn />, document.getElementById('AddNewExtensionTable'))
}
renderExtensionTable()

function viewExtensionTableColumn(name) {
    getJSON(`http://localhost:8080/ProjectAsterix/rest/extensiontable/${name}`)
        .then(data => {
            ReactDOM.render(<ExtensionTableColumns
                    extensionTableColumns={data}
                    extensionTableName={name}
                />,
                document.getElementById('ExtensionTableColumns'))
        })
}

function deleteExtensionTable(name) {
    fetch(`http://localhost:8080/ProjectAsterix/rest/extensiontable/${name}`, {
        method: 'DELETE',
        mode: 'cors'
    })
        .then(extensionTableContent)
}

function deleteExtensionTableColumn(columnInfo) {
    fetch('http://localhost:8080/ProjectAsterix/rest/columns/', {
        method: 'DELETE',
        mode: 'cors',
        headers: new Headers({
            'Content-Type': 'application/json; charset=UTF-8'
        }),
        body: JSON.stringify(columnInfo)
    })
        .then(viewExtensionTableColumn(columnInfo.tableName))
}

function addNewExtensionTableColumn(newColumnInfo) {
    fetch('http://localhost:8080/ProjectAsterix/rest/columns/', {
        method: 'POST',
        mode: 'cors',
        headers: new Headers({
            'Content-Type': 'application/json; charset=UTF-8'
        }),
        body: JSON.stringify(newColumnInfo)
    }).then(renderExtensionTable)
        .then(viewExtensionTableColumn(newColumnInfo.tableName))
}