let allData;

function LocationTypeTable(props) {
    const {locationTypes} = props
    // const locationTypes = props.locationTypes
    return (
        <table id="locationTypeTableReact">
            <thead>
            <tr>
                <th>ID</th>
                <th>Extension Table Name</th>
                <th>Description</th>
                <th>Created At</th>
                <th>Modified At</th>
                <th>Action</th>
            </tr>
            </thead>
            <tbody>
            {locationTypes.map((locationType) => (
                <tr key={locationType.id}>
                    <td>{locationType.id}</td>
                    <td>{locationType.extensionTableName}</td>
                    <td>{locationType.description}</td>
                    <td>{locationType.createdAt}</td>
                    <td>{locationType.modifiedAt}</td>
                    <td>
                        <button onClick={() => editLocationType(event)}>Edit</button>
                        <button onClick={() => deleteLocationType(locationType.id)}>Delete</button>
                    </td>
                </tr>
            ))}
            <button onClick={() => addNewLocationTypeInput(locationType.id)}>Add new location</button>
            </tbody>
        </table>

    )

}

class Row extends React.Component {
    render() {
        const {locationType} = this.props

        return (
            <tr key={locationType.id}>
                <td>{locationType.id}</td>
                <td>{locationType.extensionTableName}</td>
                <td>{locationType.description}</td>
                <td>{locationType.createdAt}</td>
                <td>{locationType.modifiedAt}</td>
                <td>
                    <button onClick={() => editLocationType(event)}>Edit</button>
                    <button onClick={() => deleteLocationType(locationType.id)}>Delete</button>
                </td>
            </tr>)
    }

}

class Buttons extends React.Component {
    render() {
        const {i} = this.props

        return (
            <div>
                <div class="editButton">
                    <button type="button" id="edit_button(${i})" class="edit" onClick="edit_row(${i})">Edit</button>
                </div>
                <div class="updateButton">
                    <button type="submit" name="updateButtonwork" id="update_button(${i})" class="update" hidden>
                        Update
                    </button>
                </div>
                <div class="deleteButton">
                    <button type="button" id="delete_button(${i})" class="delete"
                            onClick="deleteLocationType('${data[i].id}');dynamicallyDeleteLocationType(event);">Delete
                    </button>
                </div>

            </div>
        )
    }
}

function pageContent() {
    getJSON('http://localhost:8080/ProjectAsterix/rest/locationtypes/')
        .then(
            data => {
                let tableContent = "";
                allData = data;
                for (let i = 0; i < data.length; i++) {
                    const editButton = `
            <button type="button" id="edit_button(${i})" class="edit" onClick="editRow(${i})">Edit</button>
            `,
                        updateButton = `
            <button type="submit" name="updateButtonwork" id="update_button(${i})" class="update" hidden>Update</button>
            `,
                        deleteButton = `
            <button type="button" id="delete_button(${i})" class="delete"
                    onClick="deleteLocationType('${data[i].id}');dynamicallyDeleteLocationType(event);">Delete
            </button>
            `;

                    //noinspection JSUnresolvedVariable
                    tableContent += `
            <tr>
                <td>${data[i].id}</td>
                <td>${data[i].extensionTableName}</td>
                <td>${data[i].description}</td>
                <td>${data[i].createdAt}</td>
                <td>${data[i].modifiedAt}</td>
                <td>${editButton}${updateButton}${deleteButton}</td>
            </tr>
            `
                }
                getElement("locationTypeTableBody").innerHTML = tableContent;
                ReactDOM.render(<LocationTypeTable locationTypes={data}/>, document.getElementById('root'))
            });
}

pageContent()

function deleteLocationType(id) {
    //noinspection JSUnresolvedFunction
    fetch(`http://localhost:8080/ProjectAsterix/rest/locationtypes/${id}`, {
        method: 'DELETE',
        mode: 'cors'
    }).then(pageContent)
}

function dynamicallyDeleteLocationType(event) {
    const r = event.target.parentNode.parentNode.sectionRowIndex;
    document.getElementById("locationTypeTableBody").deleteRow(r);
}

function editLocationType(locationType) {
    // getElement(`edit_button(${i})`).style.display = "none";
    // getElement(`delete_button(${i})`).style.display = "none";
    // getElement(`update_button(${i})`).style.display = "block";
    const r = event.target.parentNode.parentNode.sectionRowIndex

    const row = getElement("locationTypeTableReact").rows[r],
        [id, extensionTableName, description] = row.cells;
    id.innerHTML = `<input type = "text" name = "id" id = "idUpdateValue" value = "${locationType.id}" disabled>`;
    //noinspection JSUnresolvedVariable
    extensionTableName.innerHTML = `<input type = "text" name = "extensionTableName" id = "extensionTableNameUpdateValue" value = "${locationType.extensionTableName}">`;
    description.innerHTML = `<input type = "text" name = "description" id = "descriptionUpdateValue" value = "${locationType.description}">`;

    const button = getElement(`update_button(${r})`);
    button.addEventListener("click", () => updateRow(row));
    // button.addEventListener("click", () => dynamicallyUpdateLocationType(row));
    button.addEventListener("click", dynamicallyUpdateLocationType);
}

function editRow(i) {
    getElement(`edit_button(${i})`).style.display = "none";
    getElement(`delete_button(${i})`).style.display = "none";
    getElement(`update_button(${i})`).style.display = "block";

    const row = getElement("locationTypeTableBody").rows[i],
        [id, extensionTableName, description] = row.cells;
    id.innerHTML = `<input type = "text" name = "id" id = "idUpdateValue" value = "${allData[i].id}" disabled>`;
    //noinspection JSUnresolvedVariable
    extensionTableName.innerHTML = `<input type = "text" name = "extensionTableName" id = "extensionTableNameUpdateValue" value = "${allData[i].extensionTableName}">`;
    description.innerHTML = `<input type = "text" name = "description" id = "descriptionUpdateValue" value = "${allData[i].description}">`;

    const button = getElement(`update_button(${i})`);
    button.addEventListener("click", () => updateRow(row));
    // button.addEventListener("click", () => dynamicallyUpdateLocationType(row));
    button.addEventListener("click", dynamicallyUpdateLocationType);
}

function updateRow(row) {
    const data = toJSONString(row.querySelectorAll("input"));
    //noinspection JSUnresolvedFunction
    fetch(`http://localhost:8080/ProjectAsterix/rest/locationtypes/`, {
        method: 'PUT',
        mode: 'cors',
        headers: new Headers({
            'Content-Type': 'application/json; charset=UTF-8'
        }),
        body: data
    })
        .then(checkStatus)
        .then(pageContent)
}

function dynamicallyUpdateLocationType(event) {

    const i = event.target.parentNode.parentNode.sectionRowIndex,
        row = getElement("locationTypeTableBody").rows[i],

        rowNumber = row.sectionRowIndex,
        id = getElement('idUpdateValue').value,
        extensionTableName = getElement('extensionTableNameUpdateValue').value,
        description = getElement('descriptionUpdateValue').value,
        editButton = `<button type="button" id="edit_button(${rowNumber})" class="edit" onClick="editRow(${rowNumber})">Edit</button>`,
        updateButton = `<button type="submit" name="updateButtonwork" id="update_button(${rowNumber})" class="update" hidden>Update</button>`,
        deleteButton = `<button type="button" id="delete_button(${rowNumber})" class="delete"
onClick="deleteLocationType('${id}');dynamicallyDeleteLocationType(event);">Delete</button>`;

    //noinspection JSUnresolvedVariable
    content = `
        <td>${id}</td>
        <td>${extensionTableName}</td>
        <td>${description}</td>
        <td>${allData[rowNumber].createdAt}</td>
        <td>${allData[rowNumber].modifiedAt}</td>
        <td>${editButton}${updateButton}${deleteButton}</td>
    `;
    row.innerHTML = content;
}

function addNewLocationTypeField() {
    const body = getElement("locationTypeTableBody"),
        row = body.insertRow(body.rows.length);
    row.innerHTML = `
        <td><input placeholder="ID" type="text" id="idFieldValue" name="id" required/></td>
        <td><input placeholder="Extension table name" type="text" id="extensionTableNameFieldValue" name="extensionTableName" required/></td>
        <td><input placeholder="Description" type="text" id="descriptionFieldValue" name="description" required/></td>
        <td></td>
        <td></td>
        <td><button type="submit" id="addButton" class="submit">Add</button></td>
    `;
    const button = row.querySelector('button');
    button.addEventListener("click", addNewLocationType);
    button.addEventListener("click", dynamicallyAddNewLocationType);
}

function addNewLocationType() {
    //noinspection JSUnresolvedVariable
    const data = toJSONString(locationTypeTableBody.querySelectorAll("input"));
    //noinspection JSUnresolvedFunction
    fetch(`http://localhost:8080/ProjectAsterix/rest/locationtypes/`, {
        method: 'POST',
        mode: 'cors',
        headers: new Headers({
            'Content-Type': 'application/json; charset=UTF-8'
        }),
        body: data
    })
        .then(checkStatus)
}

function dynamicallyAddNewLocationType() {
    const body = getElement("locationTypeTableBody"),
        row = body.insertRow(body.rows.length - 1),
        rowNumber = row.sectionRowIndex,
        id = getElement('idFieldValue').value,
        extensionTableName = getElement('extensionTableNameFieldValue').value,
        description = getElement('descriptionFieldValue').value,
        deleteButton = `<button type="button"  id="delete_button(${id})" class="delete"
onClick="deleteLocationType('${id}');dynamicallyDeleteLocationType(event);">Delete</button>`;
    row.innerHTML = `
        <td>${id}</td>
        <td>${extensionTableName}</td>
        <td>${description}</td>
        <td></td>
        <td></td>
        <td>${deleteButton}</td>
        `;
}





