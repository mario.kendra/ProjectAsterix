/**
 * Created by MKendra on 30/03/2017.
 */
function sisendB(b) {
    (function () { console.log('regular', this) })()
    const arrow = () => console.log(this); arrow()

    return this.a + b
}

const obj =
    {a: 3, c: sisendB}

    console.log(obj.c(5))
    console.log(sisendB.bind(obj)(5))
    console.log(sisendB.call(obj, 5))


// constructor(props) {
//     super(props);
//     this.handleChange = this.handleChange.bind(this);
//     this.state = {
//         extensionTableName: props.locationType.extensionTableName,
//         description: props.locationType.description
//     };
// }
//
// handleChange(e) {
//     this.setState({
//         extensionTableName: e.target.value,
//         description: e.target.value
//     });
// }