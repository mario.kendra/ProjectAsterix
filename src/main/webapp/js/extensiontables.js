let allData;

function ExtensionTable(props) {
    const {extensionTables} = props

    return (
        <table>
            <tbody>
            {extensionTables.map((extensionTable, i) => (
                <tr>
                    <td>{extensionTable.name}</td>
                    <td><Buttons i={i}/></td>
                </tr>
            ))}
            </tbody>
        </table>
    )
}


function webPageContent() {
    getJSON('http://localhost:8080/ProjectAsterix/rest/extensiontable/')
        .then(
            data => {
                let tableContent = "";
                allData = data;
                for (let i = 0; i < data.length; i++) {
                    const viewButton = `<button type="button" name="viewTableButton" id="view_button(${i})" class="view" onClick="viewTable('${data[i].name}')" )>View</button>`,
                        deleteButton = `<button type="button"  id="delete_button(${i})" class="delete" onClick="deleteExtensionTable('${data[i].name}');dynamicallyDeleteTable(event);">Delete</button>`;

                    tableContent += `
                            <tr>
                                <td>${data[i].name}</td>
                                <td>${viewButton}${deleteButton}</td>
                            </tr>`
                }
                getElement("extensionTablesBody").innerHTML = tableContent;
                ReactDOM.render(<ExtensionTable extensionTables={data}/>, document.getElementById('root'))
            });
}

webPageContent()

class Buttons extends React.Component {
    render() {
        const {i} = this.props

        return (
            <div>
                <div class="viewButton">
                    <button type="button" name="viewTableButton" id="view_button(${i})" class="view"
                            onClick="viewTable('${data[i].name}')">View
                    </button>
                </div>
                <div class="deleteButton">
                    <button type="button" id="delete_button(${i})" class="delete"
                            onClick="deleteExtensionTable('${data[i].name}');dynamicallyDeleteTable(event);">Delete
                    </button>
                </div>
            </div>
        )
    }
}

function addNewExtensionTableField() {
    const body = getElement("extensionTablesBody"),
        row = body.insertRow(body.rows.length);
    row.innerHTML = `
            <td><input placeholder="name" type="text" id="extensionTableFieldValue" name="name" /></td>
            <td><button type="submit" id="addButton" class="submit">Add</button></td>
            `;

    const button = row.querySelector('button');
    button.addEventListener("click", addNewExtensionTable);
    button.addEventListener("click", dynamicallyAddTable);
}

function addNewExtensionTable() {
    //noinspection JSUnresolvedVariable
    const data = toJSONString(extensionTablesBody.querySelectorAll("input"));
    //noinspection JSUnresolvedFunction
    fetch(`http://localhost:8080/ProjectAsterix/rest/extensiontable/`, {
        method: 'POST',
        mode: 'cors',
        headers: new Headers({
            'Content-Type': 'application/json; charset=UTF-8'
        }),
        body: data
    })
        .then(checkStatus)
}

function dynamicallyAddTable() {
    const body = getElement("extensionTablesBody"),
        row = body.insertRow(body.rows.length - 1),
        rowNumber = row.sectionRowIndex,
        textInputVal = getElement('extensionTableFieldValue').value,
        deleteButton = `<button type="button"  id="delete_button(${textInputVal})" class="delete" 
onClick="deleteExtensionTable('${textInputVal}');dynamicallyDeleteTable(event);">Delete</button>`;
    row.innerHTML = `
           <td>${textInputVal}</td>
           <td>${deleteButton}</td>
            `;
}

function deleteExtensionTable(name) {
    //noinspection JSUnresolvedFunction
    fetch(`http://localhost:8080/ProjectAsterix/rest/extensiontable/${name}`, {
        method: 'DELETE',
        mode: 'cors'
    })
}

function dynamicallyDeleteTable(event) {
    const r = event.target.parentNode.parentNode.sectionRowIndex;
    document.getElementById("extensionTablesBody").deleteRow(r);
}

let allColumnData;
function viewTable(name) {
    getJSON(`http://localhost:8080/ProjectAsterix/rest/extensiontable/${name}`)
        .then(
            data => {
                let tableContent = "";
                allColumnData = data;
                for (let i = 0; i < data.length; i++) {
                    const viewColumnButton = `<button type="button" name="viewColumnButton" id="view_column_button(${i})" class="view" 
onClick="editTableColumn(event)">View</button>`;

                    tableContent += `
                        <tr>
                            <td>${data[i].name}</td>
                            <td>${viewColumnButton}</td>
                            <td><input name="tableName" value="${name}" hidden disabled /></td>
                        </tr>
                        `;
                    getElement("extensionTableColumnsBody").innerHTML = tableContent;
                }
            });
}

function editTableColumn(event) {
    const i = event.target.parentNode.parentNode.sectionRowIndex,
        row = getElement("extensionTableColumnsBody").rows[i],
        [columnName, deleteButton] = row.cells;
    columnName.innerHTML = `<input type="text" name="columnName" value="${columnName.textContent}" disabled>`;
    deleteButton.innerHTML = `<button type="button" id="deleteColumnButton" class="delete">Delete</button>`;

    const button = getElement("deleteColumnButton");
    button.addEventListener("click", () => deleteFromDB(row));
    button.addEventListener("click", dynamicallyDeleteColumn);

}

function dynamicallyDeleteColumn(event) {
    const r = event.target.parentNode.parentNode.sectionRowIndex;
    document.getElementById('extensionTableColumnsBody').deleteRow(r);
}

function deleteFromDB(row) {
    const data = toJSONString(row.querySelectorAll("input"));
    //noinspection JSUnresolvedFunction
    fetch('http://localhost:8080/ProjectAsterix/rest/columns/', {
        method: 'DELETE',
        mode: 'cors',
        headers: new Headers({
            'Content-Type': 'application/json; charset=UTF-8'
        }),
        body: data
    })
        .then(checkStatus)
}

(function () {
    document.addEventListener("DOMContentLoaded", function () {
        const form = getElement("addNewColumnInput");
        form.addEventListener("submit", function (e) {
            e.preventDefault();
            const data = toJSONString(this.querySelectorAll("input, select"));
            fetch("http://localhost:8080/ProjectAsterix/rest/columns/",
                {
                    method: "POST",
                    headers: {
                        'Content-Type': 'application/json'
                    },
                    body: data
                })
        })
    });
})();

(function () {
    document.addEventListener("DOMContentLoaded", function () {
        const form = getElement("addNewColumnInput");
        form.addEventListener("submit", function (e) {
            const body = getElement("ExtensionTableColumns"),
                row = body.insertRow(body.rows.length),
                rowNumber = row.sectionRowIndex,
                columnName = getElement('columnNameFieldValue').value,
                viewColumnButton = `<button type="button" name="viewColumnButton" id="view_column_button(${rowNumber})" class="view" 
onClick="editTableColumn(event)" )>View</button>`;
            row.innerHTML = `
                    <td>${columnName}</td>
                    <td>${viewColumnButton}</td>
                    `;
        })
    });
})();

// class LastName extends React.Component {
//     render() {
//         return <h1 onClick={clickAlert}>{this.props.lastName}</h1>
//
//     }
// }
function FirstName(props) {
    return <h1 onClick={() => clickAlert(props.firstName)}>{props.firstName}</h1>
}

function LastName(props) {
    return <h1 onClick={() => clickAlert(props.lastName)}>{props.lastName}</h1>
}

function FullName(props) {
    const {firstName, lastName} = props

    return (
        <div>
            <FirstName firstName={firstName}/>
            <LastName lastName={lastName}/>
        </div>
    )
}

function clickAlert(value) {
    alert(value);
}

ReactDOM.render(
    <FullName firstName='Mario' lastName='Kendra'/>, document.getElementById('names'))