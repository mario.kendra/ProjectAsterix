/**
 * Created by MKendra on 23/03/2017.
 */
const getJSON = url => {
    return new Promise((resolve, reject) => {
        const xhr = new XMLHttpRequest();
        xhr.open('get', url, true);
        xhr.responseType = 'json';
        xhr.onload = () => {
            let status = xhr.status;
            if (status == 200) {
                resolve(xhr.response);
            } else {
                reject(status);
            }
        };
        xhr.send();
    });
};

function square(x) { return x * x } //Random test

function checkStatus(response) {
    if (response.status >= 200 && response.status <= 300) {
        return Promise.resolve(response);
    } else {
        return Promise.reject(
            new Error(response.statusText));
    }
}

function getElement(id) {
    return document.getElementById(id)
}

function toJSONString(elements) {
    const obj = {};
    for (let i = 0; i < elements.length; i++) {
        const element = elements[i],
            {name, value} = element;
        if (name)
            obj[name] = value;
    }
    return JSON.stringify(obj);
}