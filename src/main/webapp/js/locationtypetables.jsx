class LocationTypeRow extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            isEditMode: false,
            extensionTableName: props.locationType.extensionTableName,
            description: props.locationType.description
        }
        this.handleInputChange = this.handleInputChange.bind(this);
        this.handleSave = this.handleSave.bind(this);
    }

    enableEdit = () => {
        this.setState({isEditMode: true})
    }

    handleInputChange(event) {

        let change = {};
        change[event.target.name] = event.target.value;
        this.setState(change);
    }

    handleSave() {
        const editedObject = {
            "id": this.props.locationType.id,
            "extensionTableName": this.state.extensionTableName,
            "description": this.state.description
        }
        updateLocationTableRow(editedObject)
        this.setState({isEditMode: false})
    }

    render() {
        const {locationType} = this.props
        return (
            <tr>
                <td>{locationType.id}</td>
                <td>{this.state.isEditMode
                    ? <input type="text" name="extensionTableName" defaultValue={locationType.extensionTableName}
                             onChange={this.handleInputChange}/>
                    : locationType.extensionTableName}</td>
                <td>{this.state.isEditMode
                    ? <input type="text" name="description" defaultValue={locationType.description}
                             onChange={this.handleInputChange}/>
                    : locationType.description}</td>
                <td>{locationType.createdAt}</td>
                <td>{locationType.modifiedAt}</td>
                <td>
                    {this.state.isEditMode ? <button hidden/> : <button onClick={this.enableEdit}>Edit</button>}
                    {this.state.isEditMode ? <button onClick={this.handleSave}>Save</button> : <button hidden/>}
                    {this.state.isEditMode ? <button hidden/> :
                        <button onClick={() => deleteLocationType(locationType.id)}>Delete</button>}
                </td>
            </tr>
        )
    }
}

function LocationTypeTable(props) {
    const {locationTypes} = props
    return (
        <table id="locationTypeTableReact">
            <thead>
            <tr>
                <th>ID</th>
                <th>Extension Table Name</th>
                <th>Description</th>
                <th>Created At</th>
                <th>Modified At</th>
                <th>Action</th>
            </tr>
            </thead>
            <tbody>
            {locationTypes.map((locationType) => (
                <LocationTypeRow locationType={locationType}/>
            ))}
            </tbody>
        </table>
    )
}

function pageContent() {
    getJSON('http://localhost:8080/ProjectAsterix/rest/locationtypes/')
        .then(data => {
            ReactDOM.render(<LocationTypeTable locationTypes={data}/>, document.getElementById('LocationTypesTable'))
        });
}
pageContent()

class AddNewLocationType extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            id: "",
            extensiontableName: "",
            description: ""
        }
        this.handleInputChange = this.handleInputChange.bind(this);
        this.handleSave = this.handleSave.bind(this);
    }

    handleInputChange(event) {
        let change = {};
        change[event.target.name] = event.target.value;
        this.setState(change);
    }

    handleSave() {
        const newLocationTypeInfo = {
            "id": this.state.id,
            "extensionTableName": this.state.extensionTableName,
            "description": this.state.description
        }
        addNewLocationType(newLocationTypeInfo)
    }

    render() {
        return (
            <div>
                <tr>
                    <input type="text" name="id" placeholder="ID"
                           onChange={this.handleInputChange}/>
                </tr>
                <tr>
                    <input type="text" name="extensionTableName" placeholder="Extension Table Name"
                           onChange={this.handleInputChange}/>
                </tr>
                <tr>
                    <input type="text" name="description" placeholder="Description"
                           onChange={this.handleInputChange}/>
                </tr>
                <tr>
                    <button onClick={this.handleSave}>Save</button>
                </tr>
            </div>
        )
    }
}

function NewLocationTypeInput() {
    return (
        <table id="New Location Type">
            <thead>
            <tr>
                <th>Add new Location Type</th>
            </tr>
            </thead>
            <tbody>
            <AddNewLocationType/>
            </tbody>
        </table>
    )
}

function renderExtensionTable() {
    ReactDOM.render(<NewLocationTypeInput />, document.getElementById('addNewLocationType'))
}
renderExtensionTable()

function deleteLocationType(id) {
    fetch(`http://localhost:8080/ProjectAsterix/rest/locationtypes/${id}`, {
        method: 'DELETE',
        mode: 'cors'
    })
        .then(pageContent)
}

function updateLocationTableRow(editedObject) {
    fetch(`http://localhost:8080/ProjectAsterix/rest/locationtypes/`, {
        method: 'PUT',
        mode: 'cors',
        headers: new Headers({
            'Content-Type': 'application/json; charset=UTF-8'
        }),
        body: JSON.stringify(editedObject)
    })
        .then(pageContent)
}

function addNewLocationType(newLocationTypeInfo) {
    fetch(`http://localhost:8080/ProjectAsterix/rest/locationtypes/`, {
        method: 'POST',
        mode: 'cors',
        headers: new Headers({
            'Content-Type': 'application/json; charset=UTF-8'
        }),
        body: JSON.stringify(newLocationTypeInfo)
    })
        .then(pageContent)
}