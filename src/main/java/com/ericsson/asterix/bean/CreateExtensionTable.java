package com.ericsson.asterix.bean;

public class CreateExtensionTable {
    String name;

    public CreateExtensionTable() {
    }

    public CreateExtensionTable(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
