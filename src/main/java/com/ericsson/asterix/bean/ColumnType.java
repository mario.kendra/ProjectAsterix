package com.ericsson.asterix.bean;

public enum ColumnType {
    INTEGER, DECIMAL, VARCHAR, TEXT, TIMESTAMP
}