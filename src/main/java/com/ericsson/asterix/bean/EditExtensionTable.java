package com.ericsson.asterix.bean;

public class EditExtensionTable {

    private String columnName;
    String tableName;
    private ColumnType columnType;

    public EditExtensionTable() {
    }

    public EditExtensionTable(String tableName, String columnName,
                              ColumnType columnType) {
        this.tableName = tableName;
        this.columnName = columnName;
        this.columnType = columnType;
    }

    public String getColumnName() {
        return columnName;
    }

    public void setColumnName(String columnName) {
        this.columnName = columnName;
    }

    public ColumnType getColumnType() {
        return columnType;
    }

    public void setColumnType(ColumnType columnType) {
        this.columnType = columnType;
    }

    public String getTableName() {
        return tableName;
    }

    public void setTableName(String tableName) {
        this.tableName = tableName;
    }
}
