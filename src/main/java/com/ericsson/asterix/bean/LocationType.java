package com.ericsson.asterix.bean;

import java.time.Instant;

public class LocationType {

    String id;
    String extensionTableName;
    String description;
    Instant createdAt;
    Instant modifiedAt;

    public LocationType() {
    }

    public LocationType(String id, String extensionTableName,
                        String description, Instant createdAt, Instant modifiedAt) {
        this.id = id;
        this.extensionTableName = extensionTableName;
        this.description = description;
        this.createdAt = createdAt;
        this.modifiedAt = modifiedAt;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getExtensionTableName() {
        return extensionTableName;
    }

    public void setExtensionTableName(String extensionTableName) {
        this.extensionTableName = extensionTableName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Instant getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Instant createdAt) {
        this.createdAt = createdAt;
    }

    public Instant getModifiedAt() {
        return modifiedAt;
    }

    public void setModifiedAt(Instant modifiedAt) {
        this.modifiedAt = modifiedAt;
    }
}
