package com.ericsson.asterix.service;

import com.ericsson.asterix.bean.CreateExtensionTable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class CreateExtensionTableService {

    static final Logger logger = LoggerFactory
            .getLogger(CreateExtensionTableService.class);

    public CreateExtensionTableService() {
        super();
    }

    public CreateExtensionTable createExtensionTable(
            CreateExtensionTable createExtensionTable) {

        try (Connection connection = getConnectionHandling().getConnection();
             Statement statement = connection.createStatement()) {
            String createExtensionTableSQL = String.format(
                    "CREATE TABLE customer.%s()",
                    createExtensionTable.getName());
            statement.executeUpdate(createExtensionTableSQL);

        } catch (SQLException e) {
            logger.warn("Creating an extensiontable has failed", e);
        }
        return createExtensionTable;
    }


    public CreateExtensionTable deleteExtensionTable(String name) {
        CreateExtensionTable createExtensionTable = new CreateExtensionTable();

        try (Connection connection = getConnectionHandling().getConnection();
             Statement statement = connection.createStatement()) {
            String deleteExtensionTableSQL = String
                    .format("DROP TABLE customer.%s", name);
            statement.execute(deleteExtensionTableSQL);

        } catch (SQLException e) {
            logger.warn("Deleting an extensiontable has failed", e);
        }
        return createExtensionTable;
    }

    public List<CreateExtensionTable> getExtensionTableColumnNames(String name) {
        List<CreateExtensionTable> extensionTableColumnNames = new ArrayList<CreateExtensionTable>();

        try (Connection connection = getConnectionHandling().getConnection();
             PreparedStatement statement = connection.prepareStatement(
                     "SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = ?")) {
            statement.setString(1, name);
            ResultSet resultSet = statement.executeQuery();

            while (resultSet.next()) {
                extensionTableColumnNames.add(new CreateExtensionTable(
                        resultSet.getString("column_name")));
            }
        } catch (

                SQLException e)

        {
            logger.warn("Getting an extensiontable has failed", e);
        }
        return extensionTableColumnNames;
    }

    public List<CreateExtensionTable> getAllExtensionTableNames() {
        List<CreateExtensionTable> allExtensionTableNames = new ArrayList<CreateExtensionTable>();

        try (Connection connection = getConnectionHandling().getConnection();
             Statement statement = connection.createStatement()) {
            String getExtensionTableSQL = "SELECT relname AS full_rel_name FROM pg_class, pg_namespace WHERE relnamespace = pg_namespace.oid AND nspname = 'customer' AND relkind = 'r' ORDER BY full_rel_name ASC";
            ResultSet resultSet = statement.executeQuery(getExtensionTableSQL);
            {
                while (resultSet.next()) {
                    allExtensionTableNames.add(new CreateExtensionTable(
                            resultSet.getString("full_rel_name")));
                }
            }

        } catch (SQLException e) {
            logger.warn("Getting all the extension tables names has failed", e);
        }
        return allExtensionTableNames;
    }

    DBConnectionHandling getConnectionHandling() {
        return new DBConnectionHandling();
    }
}