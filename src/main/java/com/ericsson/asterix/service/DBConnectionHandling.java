package com.ericsson.asterix.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

public class DBConnectionHandling {

    static final Logger logger = LoggerFactory
            .getLogger(DBConnectionHandling.class);

    public Connection getConnection() {
        try {
            Properties prop = new Properties();
            InputStream inputStream = DBConnectionHandling.class
                    .getClassLoader().getResourceAsStream("/db.properties");
            prop.load(inputStream);
            String driver = prop.getProperty("driver");
            String url = prop.getProperty("url");
            String user = prop.getProperty("user");
            String password = prop.getProperty("password");
            Class.forName(driver);
            return DriverManager.getConnection(url, user, password);

        } catch (ClassNotFoundException | SQLException | IOException e) {
            logger.warn("Getting a connection to postgresql has failed", e);
            return null;
        }
    }

    public static void closeConnection(Connection connection) {
        try {
            connection.close();
        } catch (SQLException e) {
            logger.warn("closeConnection has failed", e);
        }
    }
}