package com.ericsson.asterix.service;

import com.ericsson.asterix.bean.LocationType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class LocationTypeService {

    static final Logger logger = LoggerFactory
            .getLogger(LocationTypeService.class);

    public LocationTypeService() {
        super();
    }

    public List<LocationType> getAllLocationTypes() {
        List<LocationType> locationTypes = new ArrayList<LocationType>();

        try (Connection connection = getConnectionHandling().getConnection();
             Statement statement = connection.createStatement();
             ResultSet resultSet = statement
                     .executeQuery("SELECT * FROM asset.location_type ORDER BY id ASC")) {
            while (resultSet.next()) {
                locationTypes.add(new LocationType(resultSet.getString("id"),
                        resultSet.getString("extension_table_name"),
                        resultSet.getString("description"),
                        resultSet.getTimestamp("created_at").toInstant(),
                        resultSet.getTimestamp("modified_at").toInstant()));
            }

        } catch (SQLException e) {
            logger.warn("Getting all the location types has failed", e);
        }
        return locationTypes;
    }

    public LocationType getLocationType(String id) {
        LocationType locationType = new LocationType();

        try (Connection connection = getConnectionHandling().getConnection();
             PreparedStatement statement = connection.prepareStatement(
                     "SELECT * FROM asset.location_type WHERE id=?")) {
            statement.setString(1, id);
            ResultSet resultSet = statement.executeQuery();

            while (resultSet.next()) {
                locationType.setId(resultSet.getString("id"));
                locationType.setExtensionTableName(
                        resultSet.getString("extension_table_name"));
                locationType.setDescription(resultSet.getString("description"));
                locationType.setCreatedAt(
                        resultSet.getTimestamp("created_at").toInstant());
                locationType.setModifiedAt(
                        resultSet.getTimestamp("modified_at").toInstant());
            }

        } catch (SQLException e) {
            logger.warn("Getting a location type has failed", e);
        }
        return locationType;
    }

    public LocationType addLocationType(LocationType locationType) {

        try (Connection connection = getConnectionHandling().getConnection();
             PreparedStatement statement = connection.prepareStatement(
                     "INSERT INTO asset.location_type(id, extension_table_name, description) VALUES (?, ?, ?)")) {
            statement.setString(1, locationType.getId());
            statement.setString(2, locationType.getExtensionTableName());
            statement.setString(3, locationType.getDescription());
            statement.executeUpdate();

        } catch (SQLException e) {
            logger.warn("Adding a location type has failed", e);
        }
        return locationType;
    }

    public LocationType deleteLocationType(String id) {
        LocationType locationType = new LocationType();

        try (Connection connection = getConnectionHandling().getConnection();

             PreparedStatement statement = connection.prepareStatement(
                     "DELETE FROM asset.location_type WHERE id=?")) {
            statement.setString(1, id);
            statement.executeUpdate();

        } catch (SQLException e) {
            logger.warn("Deleting a location type has failed", e);
        }
        return locationType;
    }

    public LocationType updateLocationType(LocationType locationType) {

        try (Connection connection = getConnectionHandling().getConnection();
             PreparedStatement statement = connection.prepareStatement(
                     "UPDATE asset.location_type SET extension_table_name=?, description=? WHERE id =?")) {
            statement.setString(1, locationType.getExtensionTableName());
            statement.setString(2, locationType.getDescription());
            statement.setString(3, locationType.getId());
            statement.executeUpdate();
        } catch (SQLException e) {
            logger.warn("Updating a location type has failed", e);
        }
        return locationType;
    }

    DBConnectionHandling getConnectionHandling() {
        return new DBConnectionHandling();
    }
}
