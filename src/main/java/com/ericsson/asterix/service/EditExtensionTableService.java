package com.ericsson.asterix.service;

import com.ericsson.asterix.bean.EditExtensionTable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

public class EditExtensionTableService {

    static final Logger logger = LoggerFactory
            .getLogger(EditExtensionTableService.class);

    public EditExtensionTableService() {
        super();
    }

    public EditExtensionTable addColumn(EditExtensionTable editExtensionTable) throws Exception {
        try (Connection connection = getConnectionHandling().getConnection();
             Statement statement = connection.createStatement()) {
            String addColumnSQL = String.format(
                    "ALTER TABLE customer.%s ADD %s %s",
                    editExtensionTable.getTableName(),
                    editExtensionTable.getColumnName(),
                    editExtensionTable.getColumnType());
            statement.executeUpdate(addColumnSQL);
        }
        return editExtensionTable;
    }

    public EditExtensionTable deleteColumn(
            EditExtensionTable editExtensionTable) {

        try (Connection connection = getConnectionHandling().getConnection();
             Statement statement = connection.createStatement()) {
            String deleteColumnSQL = String.format(
                    "ALTER TABLE customer.%s DROP COLUMN %s",
                    editExtensionTable.getTableName(),
                    editExtensionTable.getColumnName());
            statement.execute(deleteColumnSQL);
        } catch (SQLException e) {
            logger.warn("Deleting a column from this extension table failed", e);
        }
        return editExtensionTable;
    }

    DBConnectionHandling getConnectionHandling() {
        return new DBConnectionHandling();
    }
}
