package com.ericsson.asterix.controller;

import com.ericsson.asterix.bean.EditExtensionTable;
import com.ericsson.asterix.service.EditExtensionTableService;

import javax.ws.rs.DELETE;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Path("/columns")
public class EditExtensionTableController {

    EditExtensionTableService editExtensionTableService = new EditExtensionTableService();

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    public EditExtensionTable addColumn(EditExtensionTable editExtensionTable) throws Exception {
        return editExtensionTableService.addColumn(editExtensionTable);
    }

    @DELETE
    @Produces(MediaType.APPLICATION_JSON)
    public void deleteColumn(EditExtensionTable edit) {
        editExtensionTableService.deleteColumn(edit);
    }
}