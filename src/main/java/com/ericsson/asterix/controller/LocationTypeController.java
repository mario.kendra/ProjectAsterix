package com.ericsson.asterix.controller;

import com.ericsson.asterix.bean.LocationType;
import com.ericsson.asterix.service.LocationTypeService;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.List;

@Path("/locationtypes")
public class LocationTypeController {

    LocationTypeService locationTypeService = new LocationTypeService();

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<LocationType> getAllLocationTypes() {
        return locationTypeService
                .getAllLocationTypes();
    }

    @GET
    @Path("/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public LocationType getLocationTypeById(@PathParam("id") String id) {
        return locationTypeService.getLocationType(id);
    }

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    public LocationType addLocationType(LocationType locationType) {
        return locationTypeService.addLocationType(locationType);
    }

    @PUT
    @Produces(MediaType.APPLICATION_JSON)
    public LocationType updateLocationType(LocationType locationType) {
//        if (true) throw new RuntimeException("What");
        return locationTypeService.updateLocationType(locationType);
    }

    @DELETE
    @Path("/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public void deleteLocationType(@PathParam("id") String id) {
        locationTypeService.deleteLocationType(id);
    }
}
