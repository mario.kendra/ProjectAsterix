package com.ericsson.asterix.controller;

import com.ericsson.asterix.bean.CreateExtensionTable;
import com.ericsson.asterix.service.CreateExtensionTableService;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.List;

@Path("/extensiontable")
public class CreateExtensionTableController {

    CreateExtensionTableService createExtensionTableService = new CreateExtensionTableService();

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<CreateExtensionTable> getAllExtensionTableNames() {
        return createExtensionTableService
                .getAllExtensionTableNames();
    }

    @GET
    @Path("/{name}")
    @Produces(MediaType.APPLICATION_JSON)
    public List<CreateExtensionTable> getExtensionTableById(@PathParam("name") String name) {
        return createExtensionTableService.getExtensionTableColumnNames(name);
    }

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    public CreateExtensionTable CreateExtensionTable(
            CreateExtensionTable createExtensionTable) {
        return createExtensionTableService
                .createExtensionTable(createExtensionTable);
    }

    @DELETE
    @Path("/{name}")
    @Produces(MediaType.APPLICATION_JSON)
    public void deleteExtensionTable(@PathParam("name") String name) {
        createExtensionTableService.deleteExtensionTable(name);
    }

}
