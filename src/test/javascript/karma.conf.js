module.exports = function (config) {
    config.set({
        basePath: '../../..',
        frameworks: ['mocha', 'chai', 'sinon'],
        files: [
            'src/main/webapp/js/common.js',
            'src/test/javascript/*.js'

        ],
        exclude: ['src/test/javascript/karma.conf*.js'],
        reporters: ['progress'],
        port: 9876,
        logLevel: config.LOG_INFO,
        browsers: ['Chrome'],
        singleRun: false,
        autoWatch: true,
        plugins: [
            'karma-chai',
            'karma-mocha',
            'karma-chrome-launcher',
            'karma-sinon'
        ]
    });
};