it('5x5 should equal 25', function () {
    expect(square(5)).to.equal(25)
});

it('resolves', (done) => {

    const resolvingPromise = new Promise((resolve) => {
        resolve('promise resolved')
    })

    resolvingPromise.then((result) => {
        expect(result).to.equal('promise resolved')
        done()
    })
})

it('resolves as promised', function () {
    return Promise.resolve("woof")
        .then(function (m) {
            expect(m).to.equal('woof')
        })
        .catch(function (m) {
            throw new Error('was not supposed to fail')
        })
})

it('resolves as promised', function () {
    const response = {status: 200}

    return checkStatus(response)
        .then((result) => expect(result).to.equal(response))
})

it('rejects as promised', function () {
    const response = {status: 404, statusText: 'Not Found'}

    return checkStatus(response)
        .catch((result) => {
            expect(result).to.be.instanceOf(Error)
            expect(result.message).to.equal('Not Found')
        })
})

it('should call the callback function', function () {
    sinon.spy(document, 'getElementById')
    getElement('blah')
    expect(document.getElementById.calledWithExactly('blah')).to.be.true
    document.getElementById.restore()
})

it('test toJSONString', function () {
    let obj = [{"name": "aaa", "value": "bbb"}]
    let json = '{"aaa":"bbb"}'
    expect(toJSONString(obj)).to.equal(json)
})

//
// var expect = require('chai').expect,
//     getJSON = require('http://localhost:8080/ProjectAsterix/rest/locationtypes/Tallinn'),
//     sinon = require('sinon');
//
// describe('get-json-data test the request', function() {
//     global.XMLHttpRequest = sinon.useFakeXMLHttpRequest();
//
//     beforeEach(function() {
//         this.xhr = sinon.useFakeXMLHttpRequest();
//         var requests = this.requests = [];
//
//         this.xhr.onCreate = function (xhr) {
//             requests.push(xhr);
//         };
//     });
//     afterEach(function() {
//         this.xhr.restore();
//     });
//
//     it('get json data', function() {
//         var callback = sinon.spy();
//         getJSON('http://localhost:8080/ProjectAsterix/rest/locationtypes/Tallinn', callback);
//         expect(this.requests.length).to.equal(1);
//         this.requests[0].respond(200,
//             {"Content-Type": "application/json"},
//             '{"id": 1, "name": "foo"}');
//         sinon.assert.calledWith(callback, {"id": 1, "name": "foo"});
//     });
// });