package com.ericsson.asterix.service;

import com.ericsson.asterix.bean.LocationType;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;

import java.io.IOException;
import java.sql.*;
import java.time.Instant;
import java.util.List;

public class LocationTypeServiceTest {

    @Test
    public void testGetAllLocationTypes() throws SQLException, IOException {

        ResultSet resultSet = Mockito.mock(ResultSet.class);
        Instant createInstant = Instant.now();
        Instant modifyInstant = Instant.now();
        Timestamp timestampCreated = Mockito.mock(Timestamp.class);
        Timestamp timestampModified = Mockito.mock(Timestamp.class);

        Mockito.when(resultSet.next()).thenReturn(true).thenReturn(true)
                .thenReturn(false);

        Mockito.when(resultSet.getString("id")).thenReturn("Pärnu");
        Mockito.when(resultSet.getString("extension_table_name"))
                .thenReturn("Pärnu_ext");
        Mockito.when(resultSet.getString("description"))
                .thenReturn("Pärnu_description");
        Mockito.when(resultSet.getTimestamp("created_at"))
                .thenReturn(timestampCreated);
        Mockito.when(timestampCreated.toInstant()).thenReturn(createInstant);
        Mockito.when(resultSet.getTimestamp("modified_at"))
                .thenReturn(timestampModified);
        Mockito.when(timestampModified.toInstant()).thenReturn(modifyInstant);

        Statement statement = Mockito.mock(Statement.class);
        Mockito.when(
                statement.executeQuery("SELECT * FROM asset.location_type ORDER BY id ASC"))
                .thenReturn(resultSet);

        Connection connection = Mockito.mock(Connection.class);
        Mockito.when(connection.createStatement()).thenReturn(statement);

        DBConnectionHandling connectionHandling = Mockito
                .mock(DBConnectionHandling.class);
        Mockito.when(connectionHandling.getConnection()).thenReturn(connection);

        LocationTypeService service = Mockito.spy(new LocationTypeService());
        Mockito.when(service.getConnectionHandling())
                .thenReturn(connectionHandling);

        List<LocationType> locationTypes = service.getAllLocationTypes();
        Assert.assertEquals("Pärnu", locationTypes.get(0).getId());
        Assert.assertEquals("Pärnu_ext",
                locationTypes.get(0).getExtensionTableName());
        Assert.assertEquals("Pärnu_description",
                locationTypes.get(0).getDescription());
        Assert.assertSame(createInstant,
                locationTypes.get(0).getCreatedAt());
        Assert.assertSame(modifyInstant,
                locationTypes.get(0).getModifiedAt());
    }

    @Test
    public void testGetLocationType() throws SQLException, IOException {
        ResultSet resultSet = Mockito.mock(ResultSet.class);
        Instant createInstant = Instant.now();
        Instant modifyInstant = Instant.now();
        Timestamp timestampCreated = Mockito.mock(Timestamp.class);
        Timestamp timestampModified = Mockito.mock(Timestamp.class);

        Mockito.when(resultSet.next()).thenReturn(true).thenReturn(true)
                .thenReturn(false);

        Mockito.when(resultSet.getString("id")).thenReturn("Pärnu");
        Mockito.when(resultSet.getString("extension_table_name"))
                .thenReturn("Pärnu_ext");
        Mockito.when(resultSet.getString("description"))
                .thenReturn("Pärnu_description");
        Mockito.when(resultSet.getTimestamp("created_at"))
                .thenReturn(timestampCreated);
        Mockito.when(timestampCreated.toInstant()).thenReturn(createInstant);
        Mockito.when(resultSet.getTimestamp("modified_at"))
                .thenReturn(timestampModified);
        Mockito.when(timestampModified.toInstant()).thenReturn(modifyInstant);

        PreparedStatement statement = Mockito.mock(PreparedStatement.class);
        Mockito.when(statement.executeQuery()).thenReturn(resultSet);

        Connection connection = Mockito.mock(Connection.class);
        Mockito.when(connection.prepareStatement(
                "SELECT * FROM asset.location_type WHERE id=?"))
                .thenReturn(statement);

        DBConnectionHandling connectionHandling = Mockito
                .mock(DBConnectionHandling.class);
        Mockito.when(connectionHandling.getConnection()).thenReturn(connection);

        LocationTypeService service = Mockito.spy(new LocationTypeService());
        Mockito.when(service.getConnectionHandling())
                .thenReturn(connectionHandling);

        LocationType onelocationType = service.getLocationType("Pärnu");
        Assert.assertEquals("Pärnu", onelocationType.getId());
        Assert.assertEquals("Pärnu_ext",
                onelocationType.getExtensionTableName());
        Assert.assertEquals("Pärnu_description",
                onelocationType.getDescription());
        Assert.assertSame(createInstant, onelocationType.getCreatedAt());
        Assert.assertSame(modifyInstant, onelocationType.getModifiedAt());
    }
}