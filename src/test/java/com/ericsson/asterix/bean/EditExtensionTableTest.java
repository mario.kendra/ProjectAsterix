package com.ericsson.asterix.bean;

import org.junit.Assert;
import org.junit.Test;

public class EditExtensionTableTest {

    @Test
    public void testEditExtensionTableConstructor() {

        EditExtensionTable testConstructor = new EditExtensionTable("TableName",
                "ColumnName", ColumnType.INTEGER);
        Assert.assertEquals(testConstructor.getTableName(), "TableName");
        Assert.assertEquals(testConstructor.getColumnName(), "ColumnName");
        Assert.assertEquals(testConstructor.getColumnType(),
                ColumnType.INTEGER);
    }

    @Test
    public void testEditExtensionTableTableNameSetter() {
        EditExtensionTable testTableNameSetter = new EditExtensionTable();
        String testValue = "TabelName";
        testTableNameSetter.setTableName(testValue);
        String tableName = testTableNameSetter.getTableName();
        Assert.assertEquals(tableName, testValue);
    }

    @Test
    public void testEditExtensionTableColumnTypeSetter() {
        EditExtensionTable testColumnTypeSetter = new EditExtensionTable();
        ColumnType testValue = ColumnType.TIMESTAMP;
        testColumnTypeSetter.setColumnType(testValue);
        ColumnType columnType = testColumnTypeSetter.getColumnType();
        Assert.assertEquals(columnType, testValue);
    }
}
