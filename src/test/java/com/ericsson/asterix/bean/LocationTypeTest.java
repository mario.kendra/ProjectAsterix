package com.ericsson.asterix.bean;

import java.time.Instant;

import org.junit.Assert;
import org.junit.Test;

public class LocationTypeTest {

    @Test
    public void testLocationTypeConstructor() {
        LocationType testConstructor = new LocationType("Haapsalu", "Haap_ext_Table",
                "Haap_description", Instant.now(), Instant.now());
        Assert.assertEquals(testConstructor.getId(), "Haapsalu");
        Assert.assertEquals(testConstructor.getExtensionTableName(), "Haap_ext_Table");
        Assert.assertEquals(testConstructor.getDescription(), "Haap_description");
        Assert.assertEquals(testConstructor.getCreatedAt(), Instant.now());
        Assert.assertEquals(testConstructor.getModifiedAt(), Instant.now());
    }
    
    @Test
    public void testLocationTypeIdSetter() {
        LocationType testIdSetter = new LocationType();
        String testValue = "Rakvere";
        testIdSetter.setId(testValue);
        String id = testIdSetter.getId();
        Assert.assertEquals(id, testValue);
    }
    
    @Test
    public void testLocationTypeExtensionTableNameSetter() {
        LocationType testTableNameSetter = new LocationType();
        String testValue = "Rakvere_ext_Table";
        testTableNameSetter.setExtensionTableName(testValue);
        String extensionTableName = testTableNameSetter.getExtensionTableName();
        Assert.assertEquals(extensionTableName, testValue);
    }
    
    @Test
    public void testLocationTypeDescriptionSetter() {
        LocationType testDescriptionSetter = new LocationType();
        String testValue = "Rakvere_Desc";
        testDescriptionSetter.setDescription(testValue);
        String description = testDescriptionSetter.getDescription();
        Assert.assertEquals(description, testValue);
    }
}
